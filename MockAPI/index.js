const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.json());

// Simulación de base de datos
const usuarios = [
  { id: 1, username: 'usuario1', password: 'pass1', nombre: 'Usuario Uno', rol_codigo: 100, rol_nombre: 'Admin', token: 'abc123' },
  // Agrega más usuarios si es necesario
];

// Simulación de datos de informes
const informes = [
  {
    remNumero: 'REM001',
    infNumero: 1,
    fecha: new Date(2023, 3, 15),
    codProveedor: 101,
    denomProveedor: 'Proveedor A',
    observaciones: 'Ninguna',
    detalles: [
      { item: 1, codInsumo: 201, denomInsumo: 'Insumo A', cantidad: 10.5, estado: 1, lote: 'L001', fechaVencimiento: new Date(2023, 6, 20), observaciones: '' },
      // Más detalles...
    ],
  },
  // Más informes...
];

app.post('/Api/Login', bodyParser.urlencoded({ extended: true }), (req, res) => {
  const { Username, Password } = req.body;
  const usuario = usuarios.find(u => u.username === Username && u.password === Password);
  console.log(Username)
  console.log(Password)
  if (usuario) {
    res.json({ id: usuario.id, nombre: usuario.nombre, rol_codigo: usuario.rol_codigo, rol_nombre: usuario.rol_nombre, token: usuario.token });
  } else {
    res.status(401).json({ error: 'Usuario o contraseña incorrectos' });
  }
});

app.get('/Api/Informes', (req, res) => {
  // Asumimos que el token es válido (en una aplicación real deberías verificarlo)
  console.log(req);
  const { fecha_desde, fecha_hasta } = req.query;
  console.log(fecha_desde);
  console.log(fecha_hasta);
  const desde = new Date(fecha_desde);
  const hasta = new Date(fecha_hasta);
  console.log(desde);
  console.log(hasta);

  const informesFiltrados = informes.filter(informe => {
    const fechaInforme = new Date(informe.fecha);
    return fechaInforme >= desde && fechaInforme <= hasta;
  });

  res.json(informesFiltrados);
});

app.put('/Api/Informes', (req, res) => {
  // Asumimos que el token es válido
  const { infNumero, item, estado, observ } = req.query;

  const informe = informes.find(inf => inf.infNumero === parseInt(infNumero));
  if (!informe) {
    return res.status(404).json({ ok: false, error: 'Informe no encontrado' });
  }

  const detalle = informe.detalles.find(d => d.item === parseInt(item));
  if (!detalle) {
    return res.status(404).json({ ok: false, error: 'Detalle no encontrado' });
  }

  if ([1, 9].includes(estado)) {
    detalle.estado = estado;
    if (observ) {
      detalle.observaciones = observ;
    }
    res.json({ ok: true, id: `${infNumero}-${item}`, alertas: ['Actualizado correctamente'] });
  } else {
    res.status(400).json({ ok: false, error: 'Estado inválido' });
  }
});


const PORT = 3002;
app.listen(PORT, () => {
  console.log(`Servidor corriendo en el puerto ${PORT}`);
});
