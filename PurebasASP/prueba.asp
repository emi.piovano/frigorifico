<%@ Language="VBScript" %>
<%
Option Explicit

Function Login(Username, Password)
    Response.Write("Enviando solicitud a la API...<br>")
    Dim http
    Set http = Server.CreateObject("MSXML2.ServerXMLHTTP")
    Dim data
    data = "Username=" & Server.URLEncode(Username) & "&Password=" & Server.URLEncode(Password)
    Response.Write("data: " & data & "<br>")
    http.Open "POST", "http://vps-3630546-x.dattaweb.com:3002/Api/Login", False
    http.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    http.Send data
    Response.Write("Respuesta recibida. Estado: " & http.Status & ", Texto: " & http.responseText & "<br>")
    Dim token
    If http.Status = 200 Then
        Dim jsonResponse
        Set jsonResponse = ParseJSON(http.responseText)
        If Not jsonResponse Is Nothing Then
            token = jsonResponse("token")
        End If
    Else
        token = "Error: " & http.statusText
    End If

    Login = token
    Set http = Nothing
End Function

Function ObtenerInformes(Token, FechaDesde, FechaHasta)
    Response.Write("ObtenerInformes: Enviando solicitud a la API...<br>")
    Response.Write("Fecha Desde: " & FechaDesde & "<br>")
    Response.Write("Fecha Hasta: " & FechaHasta & "<br>")
    Dim http
    Set http = Server.CreateObject("MSXML2.ServerXMLHTTP")
    
    ' Construir la URL con los parámetros de consulta
    Dim url
    url = "http://localhost:3002/Api/Informes?fecha_desde=" & FechaDesde & "&fecha_hasta=" & FechaHasta
    Response.Write("URL: " & url & "<br>")

    http.Open "GET", url, False
    http.setRequestHeader "Authorization", "Bearer " & Token
    ' No es necesario establecer Content-Type para GET
    http.Send ' No se envían datos en el cuerpo de la solicitud

    Response.Write("ObtenerInformes: Respuesta recibida. Estado: " & http.Status & ", Texto: " & http.responseText & "<br>")

    Dim jsonResponse
    If http.Status = 200 Then
        Response.Write("JSON recibido: " & http.responseText & "<br>")
        Set jsonResponse = ParseJSON(http.responseText)
        If Not jsonResponse Is Nothing Then
            ObtenerInformes = jsonResponse
        Else
            ObtenerInformes = "Error: No se pudo parsear la respuesta JSON"
        End If
    Else
        ObtenerInformes = "Error: " & http.statusText
    End If

    Set http = Nothing
End Function



Function ActualizarInforme(Token, InfNumero, Item, Estado, Observaciones)
    ' Mensaje de depuración antes de enviar la solicitud
    Response.Write("ActualizarInforme: Enviando solicitud a la API...<br>")
    Dim http
    Set http = Server.CreateObject("MSXML2.ServerXMLHTTP")
    Dim data
    data = "infNumero=" & InfNumero & "&item=" & Item & "&estado=" & Estado & "&observ=" & Observaciones

    http.Open "PUT", "http://vps-3630546-x.dattaweb.com:3002/Api/Informes", False
    http.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    http.setRequestHeader "Authorization", Token
    http.Send data

    ' Mensaje de depuración después de recibir la respuesta
    Response.Write("ActualizarInforme: Respuesta recibida. Estado: " & http.Status & ", Texto: " & http.responseText & "<br>")

    Dim jsonResponse
    If http.Status = 200 Then
        Response.Write("JSON recibido: " & http.responseText & "<br>")
        Set jsonResponse = ParseJSON(http.responseText)
        If Not jsonResponse Is Nothing Then
            ActualizarInforme = jsonResponse
        Else
            ActualizarInforme = "Error: No se pudo parsear la respuesta JSON"
        End If
    Else
        ActualizarInforme = "Error: " & http.statusText
    End If

    Set http = Nothing
End Function

Function ParseJSON(jsonText)
    On Error Resume Next
    Dim jsonObj, jsonParser
    Set jsonParser = Server.CreateObject("ScriptControl")
    jsonParser.Language = "JScript"
    jsonParser.AddCode("function parseJSON(jsonText) { return JSON.parse(jsonText); }")
    Set jsonObj = jsonParser.CodeObject.parseJSON(jsonText)
    If Err.Number <> 0 Then
        Response.Write("Error al parsear JSON: " & Err.Description & "<br>")
    End If
    If Err.Number = 0 Then
        Set ParseJSON = jsonObj
    Else
        Set ParseJSON = Nothing
    End If
    On Error GoTo 0
End Function

' Variables para almacenar los resultados
Dim loginResult, obtenerInformesResult, actualizarInformeResult

' Verificar si se ha enviado el formulario
If Request.Form.Count > 0 Then
    ' Llamar a las funciones según la acción del formulario
    Select Case Request.Form("action")
        Case "login"
            loginResult = Login(Request.Form("username"), Request.Form("password"))
        Case "obtenerInformes"
            obtenerInformesResult = ObtenerInformes(Request.Form("token"), Request.Form("fechaDesde"), Request.Form("fechaHasta"))
        Case "actualizarInforme"
            actualizarInformeResult = ActualizarInforme(Request.Form("token"), Request.Form("infNumero"), Request.Form("item"), Request.Form("estado"), Request.Form("observaciones"))
    End Select
End If
%>

<html>
<head>
    <title>Prueba de API</title>
</head>
<body>
    <h1>Prueba de Funciones API</h1>

    ' Formulario para Login
    <h2>Login</h2>
    <form method="post">
        <input type="hidden" name="action" value="login">
        <label for="username">Username:</label>
        <input type="text" name="username" id="username"><br>
        <label for="password">Password:</label>
        <input type="password" name="password" id="password"><br>
        <input type="submit" value="Login">
    </form>
    <% If Not IsEmpty(loginResult) Then %>
        <p>Resultado del Login: <%= loginResult %></p>
    <% End If %>

    ' Formulario para Obtener Informes
    <h2>Obtener Informes</h2>
    <form method="post">
        <input type="hidden" name="action" value="obtenerInformes">
        <label for="token">Token:</label>
        <input type="text" name="token" id="token"><br>
        <label for="fechaDesde">Fecha Desde:</label>
        <input type="text" name="fechaDesde" id="fechaDesde"><br>
        <label for="fechaHasta">Fecha Hasta:</label>
        <input type="text" name="fechaHasta" id="fechaHasta"><br>
        <input type="submit" value="Obtener Informes">
    </form>
    <% If Not IsEmpty(obtenerInformesResult) Then %>
        <p>Resultado de Obtener Informes: <%= obtenerInformesResult %></p>
    <% End If %>

    ' Formulario para Actualizar Informe
    <h2>Actualizar Informe</h2>
    <form method="post">
        <input type="hidden" name="action" value="actualizarInforme">
        <label for="tokenUpdate">Token:</label>
        <input type="text" name="token" id="tokenUpdate"><br>
        <label for="infNumero">Número de Informe:</label>
        <input type="text" name="infNumero" id="infNumero"><br>
        <label for="item">Item:</label>
        <input type="text" name="item" id="item"><br>
        <label for="estado">Estado:</label>
        <input type="text" name="estado" id="estado"><br>
        <label for="observaciones">Observaciones:</label>
        <input type="text" name="observaciones" id="observaciones"><br>
        <input type="submit" value="Actualizar Informe">
    </form>
    <% If Not IsEmpty(actualizarInformeResult) Then %>
        <p>Resultado de Actualizar Informe: <%= actualizarInformeResult %></p>
    <% End If %>

</body>
</html>
